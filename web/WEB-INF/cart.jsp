<%--
  Created by IntelliJ IDEA.
  User: Leyla
  Date: 12/17/2020
  Time: 4:17 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Cart Application</title>
</head>
<body>
<h1>Your Cart</h1>
<table>
    <tr>
        <th>Quantity</th>
        <th>Description</th>
        <th>Price</th>
        <th>Amount</th>
        <th></th>
    </tr>

    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <c:forEach var="item" items="${cart.items}"></c:forEach>
    <tr>
        <td>
            <form action="" method="post">
                <input type="hidden" name="productCode"
                       value="<c:out value='${item.product.code}'/>"
                       <input type="text" name="quantity"
                       value="<c:out value='${item.quantity}'/>"id="quantity">
                <input type="submit" value="Update">
            </form>
        </td>
    </tr>
</table>
</body>
</html>
