<%--
  Created by IntelliJ IDEA.
  User: Leyla
  Date: 12/16/2020
  Time: 6:16 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
  </head>

  <body>
  <h1>CD list</h1>
  <table>

    <tr>
      <th>Description</th>
      <th class="right">Price</th>
      <th>&nbsp;</th>
    </tr>

    <tr>
      <td>86 (the band) - True life songs</td>
      <td class="right"> $14.5 </td>
      <td> <form action="cart" method="post">
      <input type="hidden" name="productCode" value="8601">
        <input type="submit" value="Add to Cart">
      </form>
      </td>
    </tr>

    <tr>
      <td>Paddlefoot - the CD first</td>
      <td class="right"> $12.95 </td>
      <td><form action="cart" method="post">
        <input type="hidden" name="productCode" value="pf1">
        <input type="submit" name="Add to Cart"></form></td>
    </tr>

    <tr>
      <td>Paddlefoot - the second CD</td>
      <td class="right"> $14.95 </td>
      <td><form action="cart" method="post" >
        <input type="hidden" name="productCode" value="pf02">
        <input type="submit" value="Add To Cart">
      </form></td>
    </tr>
    <tr>
      <td> Joe Rut - Genuine Wood Grained Finish</td>
      <td class="right"> $14.95 </td>
      <td><form action="cart" method="post">
        <input type="hidden" name="productCode" value="jr01">
        <input type="submit" value="Add To Cart">
      </form> </td>
    </tr>

  </table>
  </body>
</html>
